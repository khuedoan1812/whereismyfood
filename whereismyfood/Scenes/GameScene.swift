//
//  GameScene.swift
//  whereismyfood
//
//  Created by Doan van Khue on 6/17/21.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // Properties
    var ground: SKSpriteNode!
    var player: SKSpriteNode!
    var crate: SKSpriteNode!
    var object:[ Int] = []
    var walkTexture : [SKTexture] = []
    var attackTexture: [SKTexture] = []
    var coin: SKSpriteNode!
    var life: Int = 3
    var lifeNode: [SKSpriteNode] = []
    var cointIcon: SKSpriteNode!
    var gameOver = false
    
    // System
    override func didMove(to view: SKView) {
        setupNodes()
        physicsWorld.contactDelegate = self
    }
  /*
    func didBegin(_ contact: SKPhysicsContact) {
        let other = contact.bodyA.categoryBitMask == PhysicsCategory.player ? contact.bodyB : contact.bodyA
        switch other.categoryBitMask {
        case PhysicsCategory.ground:
            print("contact coin")
        default:
            break
        }
        print("delegate")
    }
   */
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self)
        let finalLocation = CGPoint(x: location.x, y: ground.frame.height - 30.0)
        let touchedNode = self.atPoint(location)
        for i in 0...walkTexture.count-1 {
            if let name = touchedNode.name
               {
                   if name == "crate_\(i)"
                   {
                    setupCoin(position: touchedNode.position.x)
                    movePlayer(location: finalLocation)
                    
                   }
               }
        }
        
        if touchedNode.name == "coin"
        {
            coin.removeFromParent()
            playerSmash()
            player.position = CGPoint(x: player.position.x, y: ground.frame.height - 30.0)
            coin.physicsBody = SKPhysicsBody(rectangleOf: coin.size)
            coin.physicsBody!.affectedByGravity = true
            coin.removeFromParent()
            setUpGameOver()
        }
        
        //movePlayer(location: location)
        
    }
    
    
}

