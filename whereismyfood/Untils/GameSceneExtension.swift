//
//  GameSceneExtension.swift
//  whereismyfood
//
//  Created by Doan van Khue on 6/17/21.
//

import SpriteKit
import GameplayKit

extension GameScene {
    
    func setupNodes() {
        createBackground()
        createGround()
        createPlayer()
        setupCrate()
        setupLife()
    }
    
    func createBackground() {
        let backGround = SKSpriteNode(imageNamed: "BG_forest")
        backGround.anchorPoint = .zero
        backGround.position = .zero
        backGround.zPosition = -1.0
        backGround.size = CGSize (width: frame.maxX, height: frame.maxY)
        addChild(backGround)
    }
    
    func createGround() {
        for i in 0...8 {
            ground = SKSpriteNode(imageNamed: "ground")
            ground.name = "ground"
            ground.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            ground.zPosition = 1.0
            ground.position = CGPoint(x: CGFloat(i)*ground.frame.width, y:0.0)
            addChild(ground)
        }
        
    }
    
    func createPlayer() {
        for i in 0...6 {
            walkTexture.append(SKTexture(imageNamed: "WALK_00\(i)"))
        }
        let firstTexture = walkTexture[0]
        player = SKSpriteNode(texture: firstTexture)
        //player = SKSpriteNode(imageNamed: "Player_000")
        player.name = "player"
        player.setScale(0.2)
        player.zPosition = 5.0
        player.position = CGPoint(x: frame.width/8.0, y: ground.frame.height - 30.0)
        //player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        //player.physicsBody!.affectedByGravity = false
        //player.physicsBody!.isDynamic = true
        addChild(player)
        
    }
    
    func setupCrate() {
        var array:[SKSpriteNode] = []
        for i in 0...2 {
            crate = SKSpriteNode(imageNamed: "Crate")
            crate.name = "crate_\(i)"
            crate.zPosition = 1.0
            crate.position = CGPoint(x: frame.width/3 + (CGFloat(i)*100), y: ground.frame.height - 28.0)
            addChild(crate)
            array.append(crate)
        }
    }
    
    func setupCoin(position: CGFloat) {
        coin = SKSpriteNode(imageNamed: "Coin_01")
        coin.name = "coin"
        coin.zPosition = 5.0
        coin.setScale(0.1)
        coin.position = CGPoint(x: position, y: ground.frame.height + 60.0)
        addChild(coin)
    }
    
    func setupLife() {
        for j in 0...2 {
            lifeNode.append(SKSpriteNode(imageNamed: "life-on"))
            setUpLifePos(lifeNode[j], i: CGFloat(j*50))
        }
    }
    
    func setUpLifePos(_ node: SKSpriteNode, i: CGFloat) {
        node.setScale(0.2)
        node.zPosition = 10.0
        node.position = CGPoint(x: i + node.frame.width, y: frame.height - node.frame.height)
        addChild(node)
    }
    
    // Game action
    func playerSmash() {
        for i in 0...6 {
            attackTexture.append(SKTexture(imageNamed: "ATTAK_00\(i)"))
        }
        player.run(SKAction.repeat(SKAction.animate(with: attackTexture, timePerFrame: 0.1, resize: true, restore: true), count: 1))
        print("player smash")
    }
    
    func playerWalk() {
        player.run(SKAction.repeatForever(SKAction.animate(with: walkTexture, timePerFrame: 0.1, resize: false, restore: true)), withKey:"walkingPlayer")
    }
    
    func movePlayer(location: CGPoint) {
        var multiplierForDirection: CGFloat
        let playerSpeed = frame.size.width / 6.0
        let moveDifferent = CGPoint(x: location.x - player.position.x, y: location.y - player.position.y)
        let distanceToMove = sqrt(moveDifferent.x * moveDifferent.x + moveDifferent.y * moveDifferent.y)
        let moveDuration = distanceToMove / playerSpeed
        
        if moveDifferent.x < 0{
            multiplierForDirection = -1.0
        }else{
            multiplierForDirection = 1.0
        }
        player.xScale = abs(player.xScale) * multiplierForDirection
        
        if player.action(forKey: "walkingPlayer") == nil {
            playerWalk()
        }
        
        let moveAction = SKAction.move(to: location, duration: TimeInterval(moveDuration))
        let doneAction = SKAction.run({[weak self] in self?.playerMoveToTarget()})
        let moveActionWithDone = SKAction.sequence([moveAction,doneAction])
        player.run(moveActionWithDone, withKey: "playerMoving")
    }
    
    func playerMoveToTarget() {
        player.removeAllActions()
    }
    
    func setUpGameOver() {
        life -= 1
        if life <= 0 { life = 0 }
        lifeNode[life].texture = SKTexture(imageNamed: "life-off")
        print(life)
    }
    
}
