//
//  PhysicBody.swift
//  whereismyfood
//
//  Created by Doan van Khue on 6/23/21.
//

import Foundation

struct PhysicsCategory {
    static let player: UInt32 = 0b1
    static let ground: UInt32 = 0b10
    static let coin: UInt32 = 0b100
    static let zombie: UInt32 = 0b1000
}
